+++
title = "Apéro dessine-moi un jardin"
description = "Venez partager un goûter et participer à des ateliers autour de la paysannerie !"
[extra]
event_date = 2024-07-19T18:00:00
+++

# Goûter au champ

Apero Dessine moi un jardin à partir de 18h sur le terrain de Jean Guillaume
