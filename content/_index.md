+++
title = "Accueil"
template = "custom/1-home.html"
+++

# L'oignon fait la force

Nous proposons des événements pour
renouer avec les savoirs et les pratiques de la paysannerie,
sensibiliser à une agriculture protectrice du vivant,
dans une démarche d'éducation populaire.

<div class="img-fixed img-homepage alignfull" style="background-image: url(/images/fauchage_juin_24.jpeg);">
</div>